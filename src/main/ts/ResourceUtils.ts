import Container from "@yinz/container/ts/Container";
import Logger from "@yinz/commons/ts/Logger";
import TypeormDao from "@yinz/commons.data/ts/TypeormDao";
import Model from "@yinz/commons.data/ts/Model";
import Asserts from "@yinz/commons/ts/Asserts";
import { YinzOptions } from "@yinz/commons/ts/YinzOptions";
import { ModelOrderOp } from "@yinz/commons.data/ts/FilterTypes";
import * as _ from "lodash";


export interface ResourceUtilsOptions {
    container: Container;
    logger: Logger

}

export default class ResourceUtils {

    private _container: Container;
    private _logger: Logger;

    constructor(options: ResourceUtilsOptions) {

        this._container = options.container;
        this._logger = options.logger;

    }

    public apiEmbed2DaoInclude(apiEmbed, resource: any, options?: YinzOptions) { 

        options = options || {};

        let result = {...options};

        if ( apiEmbed || resource ) {
            result.include = result.include || [];
            result.include = _.isArray(result.include) ? result.include : [result.include];            
        }

        // TODO either here or in the api, we should prevent the user from going any deeper than one level in associtions
        // otherwise it could cause some serious securtiy issues...
        // ex:
        // * user has a profile
        // * Get localhost:/user/1/?embed_profiles.user
        // * returns user with the specified id + its profiles + ALL USERS WHO HAVE THE SAME PROFILE.
        // Generaly this type of service should be granted TO ONLY ADMIN, because in normal use cases
        // we have a lot of business assertion to do. like only being able to crud own resources etc...
        // but for now we will ensure this in the api, we will filter the fields which have a dot `.` in them.

        // don't add resources of `embed` that are already in `include`        
        if ( apiEmbed ) {            
            for (let item of apiEmbed) {
                if ( result.include && !result.include.includes(item) ) {
                    result.include.push(item);
                }
            }
        }

        // add embedded objects (object: a property that is a plain object with `id`)        
        if ( resource ) {
            
            for ( let propName in resource ) {

                // don't add embedded resources that are already in `include`
                let prop = resource[propName];
                if ( result.include && result.include.includes(propName)) {
                    continue;
                }
                
                // add objects that have an `id`
                if ( _.isPlainObject(prop) && prop.hasOwnProperty('id') && result.include) {
                    this._logger.info('     ResourcesUTils.apiEmbed ---> we found it ', propName)
                    result.include.push(propName);
                    continue;
                }                
                // if relation is an array, check that we have an id for each element included
                if ( _.isArray(prop) ) {                    
                    for ( let elem of prop) {                        
                        if (_.isPlainObject(elem) && elem.hasOwnProperty('id') && result.include ) {
                            this._logger.info('     ResourcesUTils.apiEmbed ---> we found it ', propName)
                            result.include.push(propName);
                            continue;
                        }
                    }                    
                }

                // TODO add arrays that have at least one object that has an `id`
            }
        }

        // if resource has a fields which is an object with an id  

        return result


    }

    public apiFilter2DaoFilter(apiFilter): any {
        
        if (!apiFilter) {
            return {};
        }

        let daoFilter = {};
        
        for (let filterName in apiFilter ) {            

            if ( !apiFilter.hasOwnProperty(filterName) ) {
                continue;
            }

            if ( !_.isPlainObject(apiFilter[filterName]) ) {
                daoFilter[filterName] = apiFilter[filterName];
                continue;
            }

            let opName = Object.keys(apiFilter[filterName])[0]; // for the moment, we will use only the first prop (too bad)

            // if non of they keys contains any op name, we will pass the object as it is
            const opNames = ['ne', 'lt', 'lte', 'gt', 'gte', 'like', 'between', 'in']
            let found = false;
            for (let key of Object.keys(apiFilter[filterName])) {
                if (opNames.includes(key)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                daoFilter[filterName] = apiFilter[filterName];
                continue;
            }

            let tmp: any = {};

            switch (opName) {
                // case 'ne'     : tmp.op = '!='     ; break;
                case 'lt'     : tmp.op = '<'      ; break;
                case 'lte'    : tmp.op = '<='     ; break;
                case 'gt'     : tmp.op = '>'      ; break;
                case 'gte'    : tmp.op = '>='     ; break;
                case 'like'   : tmp.op = 'LIKE'   ; break;
                case 'between': tmp.op = 'BETWEEN'; break;
                case 'in'     : tmp.op = 'IN'     ; break;
            }

            tmp.val = apiFilter[filterName][opName];

            daoFilter[filterName] = tmp
        }

        return daoFilter;
    }

    public apiOrder2DaoOrder(apiOrder: any, options?: YinzOptions & { orderBy?: ModelOrderOp<any>}): any {


        options = options || {};

        if (!apiOrder) {
            return apiOrder;
        }

        Asserts.isArray(apiOrder, 'ER_RES_UTL__ORD__INV_ORD', {
            message: `Invalid resource order [${apiOrder}]! It should be a none empty array.`
        });
        Asserts.isFalsy( (apiOrder.length < 1), 'ER_RES_UTL__ORD__EMPTY_ORD', {
            message: `Invalid resource order [${apiOrder}]! It should be a none empty array.`
        });
        
        options.orderBy = options.orderBy || {}

        // make sure the dao order is always DESC or ASC
        for ( let elem of apiOrder ) {

            for ( let key in elem) {
                options.orderBy[key] = elem[key] === "ASC" || 'asc' ? "ASC" : elem[key] === "DESC" || "desc" ? "DESC" : "ASC";            
            }            
        }

        return options;

    }

    public lookupDao(resourceName: string): TypeormDao<Model> {

        this._logger.trace('Enter ResourceUtils.lookupDao(%s) --> {', resourceName);

        // make sure that resourceName is good enough to work with
        Asserts.isNonEmptyString(resourceName, 'ERR_RES_UTL__LKUP_DAO__INV_RES_NAME', {
            message: `util.format(Invalid resource name [${resourceName}]! It should be a none empty string.'`,
        });

        // dao name 

        let dao = this._container.getBean<TypeormDao<Model>>(resourceName + 'Dao');

        // make sure that we have a dao to use
        Asserts.isTruthy(dao, 'ERR_RES_UTL__LKUP_DAO__NOT_FOUND', {
            message: `The dao of resource [${resourceName}] not found!`
        });

        this._logger.trace('Leave ResourceUtils.lookupDao(%j) --> {', dao);
        return dao;
    }

}