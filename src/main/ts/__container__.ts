import Container from "@yinz/container/ts/Container";
import { Logger } from "@yinz/commons";
import AccessManager from "@yinz/access.data/ts/AccessManager";
import CreateResourceService from "./CreateResourceService";
import ResourceUtils from "./ResourceUtils";
import LookupResourceService from "./LookupResourceService";
import UpdateResourceService from "./UpdateResourceService";
import DeleteResourceService from "./DeleteResourceService";
import LookupResourcesService from "./LookupResourcesService";




const registerBeans = (container: Container) => {

    let resourceUtils = new ResourceUtils({
        logger: container.getBean<Logger>('logger'),
        container
    })

    let createResourceService = new CreateResourceService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        resourceUtils                
    });

    let updateResourceService = new UpdateResourceService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        resourceUtils
    });

    let deleteResourceService = new DeleteResourceService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        resourceUtils
    });

    let lookupResourceService = new LookupResourceService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        resourceUtils
    });

    let lookupResourcesService = new LookupResourcesService({
        logger: container.getBean<Logger>('logger'),
        accessManager: container.getBean<AccessManager>('accessManager'),
        resourceUtils
    });

    
    container.setBean('lookupResourcesService', lookupResourcesService);
    container.setBean('createResourceService', createResourceService);
    container.setBean('lookupResourceService', lookupResourceService);    
    container.setBean('updateResourceService', updateResourceService);
    container.setBean('deleteResourceService', deleteResourceService);
    container.setBean('resourceUtils', resourceUtils);
    

};

const registerClazzes = (container: Container) => {

    container.setClazz('ResourceUtils', ResourceUtils);
    container.setClazz('CreateResourceService', CreateResourceService);
    container.setClazz('LookupResourceService', LookupResourceService);
    container.setClazz('UpdateResourceService', UpdateResourceService);
    container.setClazz('DeleteResourceService', DeleteResourceService);
    container.setClazz('LookupResourcesService', LookupResourcesService);

};


export {
    registerBeans,
    registerClazzes
};