import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate"
import ResourceUtils from "./ResourceUtils";
import { YinzOptions, Asserts } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";


export interface CreateResourceServiceReq extends DefaultServiceReq {
    resourceName: string;
    resource: any;
    embed?: any;
}

export interface CreateResourceServiceRsp extends DefaultServiceRsp {
    resource: any;
}

export interface CreateResourceServiceOptions extends ServiceTemplateOptions {
    resourceUtils: ResourceUtils;
}

export default class CreateResourceService extends ServiceTemplate<CreateResourceServiceReq, CreateResourceServiceRsp> {

    private _resouceUtils: ResourceUtils;

    constructor(options: CreateResourceServiceOptions) {

        options.logReqRsp = false;
        options.serviceName = "CreateResourceService";

        super(options);

        this._resouceUtils = options.resourceUtils;

    }

    protected valReqRemFlds(req: CreateResourceServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // 1. resourceName
        Asserts.isNonEmptyString(req.resourceName, 'ERR_CREATE_RES_SRV__MISS_RES_NAME', {
            message: '`req.resourceName` is missing! It should be a none-empty string',
            resourceName: req.resourceName,
            req: req,
        });

        // 2. resource
        Asserts.isTruthy(req.resource, 'ERR_CREATE_RES_SRV__MISS_RES', {
            message: '`req.resource` is missing!',
            resource: req.resource,
            req: req,
        });

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: CreateResourceServiceReq, options?: YinzOptions): Promise<CreateResourceServiceRsp> {

        let resourceName = req.resourceName;

        let dao = this._resouceUtils.lookupDao(resourceName);

        // convert API embed to dao include
        let createOptions = this._resouceUtils.apiEmbed2DaoInclude(req.embed, req.resource, options);
        createOptions.user = req.reqUser;
        createOptions.__$$actionIsGranted$$__ = false; // remove implicit grant assigned once service is granted.

        this._logger.info(`About to create an instance of ${resourceName}...`);

        let resource = await dao.create(handler, req.resource, createOptions);

        return { resource };
    }
}