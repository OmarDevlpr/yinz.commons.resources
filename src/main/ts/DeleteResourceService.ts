import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate"
import ResourceUtils from "./ResourceUtils";
import { YinzOptions, Asserts } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";


export interface DeleteResourceServiceReq extends DefaultServiceReq {
    resourceName: string;    
    id: number;
    embed?: any;
}

export interface DeleteResourceServiceRsp extends DefaultServiceRsp {
    resource: any;
}

export interface DeleteResourceServiceOptions extends ServiceTemplateOptions {
    resourceUtils: ResourceUtils;
}

export default class DeleteResourceService extends ServiceTemplate<DeleteResourceServiceReq, DeleteResourceServiceRsp> {

    private _resouceUtils: ResourceUtils;

    constructor(options: DeleteResourceServiceOptions) {

        options.logReqRsp = false;
        options.serviceName = "DeleteResourceService";

        super(options);

        this._resouceUtils = options.resourceUtils;

    }

    protected valReqRemFlds(req: DeleteResourceServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // 1. resourceName
        Asserts.isNonEmptyString(req.resourceName, 'ERR_DELETE_RES_SRV__MISS_RES_NAME', {
            message: '`req.resourceName` is missing! It should be a none-empty string',
            resourceName: req.resourceName,
            req: req,
        });
        
        // 2. resource.id
        Asserts.isTruthy(req.id, 'ERR_DELETE_RES_SRV__MISS_ID', {
            message: '`req.id` is missing!',
            id: req.id,
            req: req,
        });

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: DeleteResourceServiceReq, options?: YinzOptions): Promise<DeleteResourceServiceRsp> {

        let resourceName = req.resourceName;

        let dao = this._resouceUtils.lookupDao(resourceName);

        // convert API embed to dao include
        let updateOptions = this._resouceUtils.apiEmbed2DaoInclude(req.embed, null, options);
        updateOptions.user = req.reqUser;
        updateOptions.__$$actionIsGranted$$__ = false; // remove implicit grant assigned once service is granted.

        this._logger.info(`About to delete an instance of ${resourceName}...`);

        await dao.deleteById(handler, req.id, updateOptions);
    
        return { resource: {id: req.id} };
    }
}