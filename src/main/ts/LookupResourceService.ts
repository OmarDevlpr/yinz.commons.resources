import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate"
import ResourceUtils from "./ResourceUtils";
import { YinzOptions, Asserts } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";


export interface LookupResourceServiceReq extends DefaultServiceReq {
    resourceName: string;
    id: number;
    embed?: any;
}

export interface LookupResourceServiceRsp extends DefaultServiceRsp {
    resource: any;
}

export interface LookupResourceServiceOptions extends ServiceTemplateOptions {
    resourceUtils: ResourceUtils;
}

export default class LookupResourceService extends ServiceTemplate<LookupResourceServiceReq, LookupResourceServiceRsp> {

    private _resouceUtils: ResourceUtils;

    constructor(options: LookupResourceServiceOptions) {

        options.logReqRsp = false;
        options.serviceName = "LookupResourceService";

        super(options);

        this._resouceUtils = options.resourceUtils;

    }

    protected valReqRemFlds(req: LookupResourceServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // 1. resourceName
        Asserts.isNonEmptyString(req.resourceName, 'ERR_LKUP_RES_SRV__MISS_RES_NAME', {
            message: '`req.resourceName` is missing! It should be a none-empty string',
            resourceName: req.resourceName,
            req: req,
        });

        // 2. resource
        Asserts.isFinite(req.id, 'ERR_LKUP_RES_SRV__INV_ID', {
            message: '`req.id` should be a finit integer!',
            id: req.id,
            req: req,
        });

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: LookupResourceServiceReq, options?: YinzOptions): Promise<LookupResourceServiceRsp> {

        let resourceName = req.resourceName;

        let dao = this._resouceUtils.lookupDao(resourceName);

        // convert API embed to dao include
        let createOptions = this._resouceUtils.apiEmbed2DaoInclude(req.embed, null, options);
        createOptions.user = req.reqUser;
        createOptions.__$$actionIsGranted$$__ = false; // remove implicit grant assigned once service is granted.

        this._logger.info(`About to lookup an instance of ${resourceName}...`);

        let resource = await dao.findById(handler, req.id, createOptions);

        return { resource };
    }

    protected async translateNegativeRsp(handler: YinzConnection | YinzTransaction, req: LookupResourceServiceReq,  rsp: LookupResourceServiceRsp, options?: YinzOptions): Promise<LookupResourceServiceRsp> {

        // call parent method
        await super.translateNegativeRsp(handler, req, rsp, options);

        if (rsp && rsp.rspReason && rsp.rspReason.match(/_DATA_NOT_FOUND$/)) {
            rsp.rspReason = 'ERR_LKP_RES_SRV__TNSLT_NEG_RSP__RES_NOT_FOUND';
            rsp.rspMessage = `No resource '${req.resourceName}' with id '${req.id}' exist!`
        }
        
        return rsp;

    }
}