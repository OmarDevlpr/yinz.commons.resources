import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate"
import ResourceUtils from "./ResourceUtils";
import { YinzOptions, Asserts } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";


export interface LookupResourcesServiceReq extends DefaultServiceReq {
    resourceName: string;
    order?: any[];    
    embed?: any;
    filter?: any;
}

export interface LookupResourcesServiceRsp extends DefaultServiceRsp {
    resources: any;
}

export interface LookupResourcesServiceOptions extends ServiceTemplateOptions {
    resourceUtils: ResourceUtils;
}

export default class LookupResourcesService extends ServiceTemplate<LookupResourcesServiceReq, LookupResourcesServiceRsp> {

    private _resouceUtils: ResourceUtils;

    constructor(options: LookupResourcesServiceOptions) {

        options.logReqRsp = false;
        options.serviceName = "LookupResourcesService";

        super(options);

        this._resouceUtils = options.resourceUtils;

    }

    protected valReqRemFlds(req: LookupResourcesServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // 1. resourceName
        Asserts.isNonEmptyString(req.resourceName, 'ERR_LKUP_RES_SRV__MISS_RES_NAME', {
            message: '`req.resourceName` is missing! It should be a none-empty string',
            resourceName: req.resourceName,
            req: req,
        });

        // 2. order        
        if ( req.order ) {
            Asserts.isArray(req.order, 'ERR_LKP_RES_SRV__VAL_FLDS__INV_ORD', {
                message: `Invalid resource order [${req.order}]! It should be a none empty array.`
            });
            Asserts.isTruthy( req.order.length > 0, 'ERR_LKP_RES_SRV__VAL_FLDS__EMPTY_ORD', {
                message: `Invalid resource order [${req.order}]! It should be a none empty array.`
            });
        }        

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: LookupResourcesServiceReq, options?: YinzOptions): Promise<LookupResourcesServiceRsp> {

        let resourceName = req.resourceName;

        // dao 
        let dao = this._resouceUtils.lookupDao(resourceName);


        // convert API filter to dao filter
        let filter = this._resouceUtils.apiFilter2DaoFilter(req.filter)        
        this._logger.debug('filter -->', filter)

        // convert API embed to dao include
        let findOptions = this._resouceUtils.apiEmbed2DaoInclude(req.embed, null, options);
        findOptions.user = req.reqUser;
        findOptions.__$$actionIsGranted$$__ = false; // remove implicit grant assigned once service is granted.


        // convert API order to dao order
        this._resouceUtils.apiOrder2DaoOrder(req.order, options);

        this._logger.info(`About to lookup instances of ${resourceName}...`);

        let resources = await dao.findByFilter(handler, filter, findOptions);

        return { resources };
    }

}