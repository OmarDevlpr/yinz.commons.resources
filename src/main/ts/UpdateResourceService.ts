import ServiceTemplate, { DefaultServiceReq, DefaultServiceRsp, ServiceTemplateOptions } from "@yinz/commons.services/ts/ServiceTemplate"
import ResourceUtils from "./ResourceUtils";
import { YinzOptions, Asserts } from "@yinz/commons";
import { YinzTransaction, YinzConnection } from "@yinz/commons.data/ts/DataSource";


export interface UpdateResourceServiceReq extends DefaultServiceReq {
    resourceName: string;
    resource: any;
    id: number;
    embed?: any;
}

export interface UpdateResourceServiceRsp extends DefaultServiceRsp {
    resource: any;
}

export interface UpdateResourceServiceOptions extends ServiceTemplateOptions {
    resourceUtils: ResourceUtils;
}

export default class UpdateResourceService extends ServiceTemplate<UpdateResourceServiceReq, UpdateResourceServiceRsp> {

    private _resouceUtils: ResourceUtils;

    constructor(options: UpdateResourceServiceOptions) {

        options.logReqRsp = false;
        options.serviceName = "UpdateResourceService";

        super(options);

        this._resouceUtils = options.resourceUtils;

    }

    protected valReqRemFlds(req: UpdateResourceServiceReq, options?: YinzOptions): void {

        super.valReqRemFlds(req, options);

        // 1. resourceName
        Asserts.isNonEmptyString(req.resourceName, 'ERR_UPDATE_RES_SRV__MISS_RES_NAME', {
            message: '`req.resourceName` is missing! It should be a none-empty string',
            resourceName: req.resourceName,
            req: req,
        });

        // 2. resource
        Asserts.isTruthy(req.resource, 'ERR_UPDATE_RES_SRV__MISS_RES', {
            message: '`req.resource` is missing!',
            resource: req.resource,
            req: req,
        });

        // 2. resource.id
        Asserts.isTruthy(req.id, 'ERR_UPDATE_RES_SRV__MISS_ID', {
            message: '`req.id` is missing!',
            id: req.id,
            req: req,
        });

    }


    protected async executeService(handler: YinzConnection | YinzTransaction, req: UpdateResourceServiceReq, options?: YinzOptions): Promise<UpdateResourceServiceRsp> {

        let resourceName = req.resourceName;

        let dao = this._resouceUtils.lookupDao(resourceName);

        // convert API embed to dao include
        let updateOptions = this._resouceUtils.apiEmbed2DaoInclude(req.embed, req.resource, options);
        updateOptions.user = req.reqUser;
        updateOptions.__$$actionIsGranted$$__ = false; // remove implicit grant assigned once service is granted.

        this._logger.info(`About to update an instance of ${resourceName}...`);

        await dao.updateById(handler, req.resource, req.id, updateOptions);
        
        req.resource.id = req.id;

        return { resource: req.resource };
    }
}