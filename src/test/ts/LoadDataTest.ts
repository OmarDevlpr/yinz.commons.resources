import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";
import DataLoader from "@yinz/tools.dataloader/ts/DataLoader"
import {  TypeormDataSource, TypeormDao } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/tools.dataloader/ts',
        process.cwd() + '/dist/main/ts'
    ],
});


const dataLoader = container.getBean<DataLoader>('dataLoader');
const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');

let conn: YinzConnection;
let trans: YinzTransaction;


describe('| token.data.loadData', function () {

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);
            dataLoader.resetCache()
            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            dataLoader.resetCache()
            resolve(true);
        })
    });

    it('| should load authzes', async () => {

        // 1. test data
        const fileNames = './src/main/data/__module_data__.yml'
        const options = {user: "__super_user__"}

        // 2. exec test
        const result = await dataLoader.load(trans, fileNames, options)

        // 3. check result  
        // 3.1 check load result
        assert.ok(result);
        assert.strictEqual(Object.keys(result).length, 1);

        assert.ok(result.authz, stringify(result));
        assert.ok(result.authz.recs);
        assert.strictEqual(result.authz.recs.total, 5);
        assert.strictEqual(result.authz.recs.loaded, 5);


        
        // 3.2 check db records
        assert.strictEqual( (await authzDao.findAll(trans, options)).length, 5)         
      
    });
});
