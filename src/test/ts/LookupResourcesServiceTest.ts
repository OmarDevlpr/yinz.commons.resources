// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import Profile from "@yinz/access.data/models/Profile";
import User from "@yinz/access.data/models/User";
import AuthzGroup from "@yinz/access.data/models/AuthzGroup";
import ProfileAuthz from "@yinz/access.data/models/ProfileAuthz";
import LookupResourcesService from "../../main/ts/LookupResourcesService";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const authzGroupDao = container.getBean<TypeormDao<AuthzGroup>>('authzGroupDao');
const userDao = container.getBean<TypeormDao<User>>('userDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
const profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');

const lookupResourcesService = container.getBean<LookupResourcesService>("lookupResourcesService");


let conn: YinzConnection;
let trans: YinzTransaction;

// authz
let executeServiceAuthz; // = { code: 'CreateResourceService' } as Authz;
let createResourceAuthz; // = { code: 'AuthzGroup' } as Authz;
let createResourceUser; // = { code: 'AuthzGroup' } as Authz;

// profiles
let grantService // = { code: chance.string({ length: 10 }) } as Profile;
let grantDao // = { code: chance.string({ length: 10 }) } as Profile;
let grantNone // = { code: chance.string({ length: 10 }) } as Profile;

let user /* = {
    code: chance.string({ length: 10 }),
    status: chance.pick(['A', 'I', 'L']),
} as User; */

const createAuthzNdProfiles = async (trans) => {

    // init 
    // authz
    executeServiceAuthz = { code: 'LookupResourcesService' } as Authz;
    createResourceAuthz = { code: 'AuthzGroup' } as Authz;
    createResourceUser = { code: 'User' } as User;

    // profiles
    grantService = { code: chance.string({ length: 10 }) } as Profile;
    grantDao = { code: chance.string({ length: 10 }) } as Profile;
    grantNone = { code: chance.string({ length: 10 }) } as Profile;

    user = {
        code: chance.string({ length: 10 }),
        status: chance.pick(['A', 'I', 'L']),
    } as User;

    // create authzs
    executeServiceAuthz = await authzDao.create(trans, executeServiceAuthz, { user: "__super_user__" });
    createResourceAuthz = await authzDao.create(trans, createResourceAuthz, { user: "__super_user__" });
    createResourceUser = await authzDao.create(trans, createResourceUser, { user: "__super_user__" });

    // create profiles
    grantService = await profileDao.create(trans, grantService, { user: "__super_user__", include: ['authzs'] });
    grantDao = await profileDao.create(trans, grantDao, { user: "__super_user__", include: ['authzs'] });
    grantNone = await profileDao.create(trans, grantNone, { user: "__super_user__", include: ['authzs'] });

    // grant service    
    grantService.profileAuthzs = createResourceAuthz.profileAuthzs = [(await profileAuthzDao.create(trans, {
        profile: grantService,
        authz: executeServiceAuthz,
        allowAll: true
    }, { user: "__super_user__", include: ['profile', 'authz'] }))]

    // grant dao    
    grantDao.profileAuthzs = createResourceAuthz.profileAuthzs = (await profileAuthzDao.createAll(trans, [
        { profile: grantDao, authz: createResourceAuthz, allowAll: true },
        { profile: grantDao, authz: createResourceUser, allowAll: true },
    ], { user: "__super_user__", include: ['profile', 'authz'] }))

}

describe('| commons.resources <LookupResourcesService>', function () {




    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);

            await createAuthzNdProfiles(trans);

            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });



    it('\n' +
        '       | Conditions: \n' +        
        '       | - resourceName is valid \n' +
        '       | - reqUser is valid  \n' +
        '       | - req.order is valid  \n' +
        '       | - req.filter is valid  \n' +
        '       | - user is allowed to execute service  \n' +
        '       | - user is allowed to create resource  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should return the resource.  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create user
            user.profiles = [grantService, grantDao]

            await userDao.create(trans, user, { user: "__super_user__", include: ['profiles'] });

            // await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']})                        

            let authzGroup = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup2 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup3 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup4 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            authzGroup = await authzGroupDao.create(trans, authzGroup, { user: "__super_user__" });
            authzGroup2 = await authzGroupDao.create(trans, authzGroup2, { user: "__super_user__" });
            authzGroup3 = await authzGroupDao.create(trans, authzGroup3, { user: "__super_user__" });
            authzGroup4 = await authzGroupDao.create(trans, authzGroup4, { user: "__super_user__" });

            // 1.2 req
            let req = {
                resourceName: "authzGroup",                
                reqUser: user.code,
                order: [{id: "ASC"}],
                filter: {code: {in: [authzGroup.code, authzGroup2.code, authzGroup3.code]}}
            }

            // 1.3 options
            let createResourceOpts = {}

            // 2. execute test

            let rsp = await lookupResourcesService.process(trans, req, createResourceOpts);

            // 3. assert result 
            // 3.1 check returned rsp

            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, 'A')            
            assert.ok(rsp.resources)            
            assert.strictEqual(rsp.resources.length, 3);
            
            let previous: any = null;
            for ( let resource of rsp.resources ) {

                if ( previous ) {

                    assert.ok(resource.id > previous.id)
                } 

                previous = resource;                
            }

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - resourceName is valid \n' +
        '       | - reqUser is valid  \n' +
        '       | - req.order is valid  \n' +
        '       | - req.filter is empty object  \n' +
        '       | - user is allowed to execute service  \n' +
        '       | - user is allowed to create resource  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should return the resource.  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create user
            user.profiles = [grantService, grantDao]

            await userDao.create(trans, user, { user: "__super_user__", include: ['profiles'] });

            // await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']})                        

            let authzGroup = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup2 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup3 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup4 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            authzGroup = await authzGroupDao.create(trans, authzGroup, { user: "__super_user__" });
            authzGroup2 = await authzGroupDao.create(trans, authzGroup2, { user: "__super_user__" });
            authzGroup3 = await authzGroupDao.create(trans, authzGroup3, { user: "__super_user__" });
            authzGroup4 = await authzGroupDao.create(trans, authzGroup4, { user: "__super_user__" });

            // 1.2 req
            let req = {
                resourceName: "authzGroup",
                reqUser: user.code,
                order: [{ id: "ASC" }],
                filter: {}
            }

            // 1.3 options
            let createResourceOpts = {}

            // 2. execute test

            let rsp = await lookupResourcesService.process(trans, req, createResourceOpts);

            // 3. assert result 
            // 3.1 check returned rsp

            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, 'A')            
            assert.ok(rsp.resources)
            assert.strictEqual(rsp.resources.length, 4);

            let previous: any = null;
            for (let resource of rsp.resources) {

                if (previous) {

                    assert.ok(resource.id > previous.id)
                }

                previous = resource;
            }

        });
  

    it('\n' +
        '       | Conditions: \n' +
        '       | - resourceName is valid \n' +
        '       | - reqUser is valid  \n' +
        '       | - req.order is valid  \n' +
        '       | - req.filter is empty null  \n' +
        '       | - user is allowed to execute service  \n' +
        '       | - user is allowed to create resource  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should return the resource.  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create user
            user.profiles = [grantService, grantDao]

            await userDao.create(trans, user, { user: "__super_user__", include: ['profiles'] });

            // await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']})                        

            let authzGroup = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup2 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup3 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            let authzGroup4 = { code: chance.string({ length: 10 }) } as AuthzGroup;
            authzGroup = await authzGroupDao.create(trans, authzGroup, { user: "__super_user__" });
            authzGroup2 = await authzGroupDao.create(trans, authzGroup2, { user: "__super_user__" });
            authzGroup3 = await authzGroupDao.create(trans, authzGroup3, { user: "__super_user__" });
            authzGroup4 = await authzGroupDao.create(trans, authzGroup4, { user: "__super_user__" });

            // 1.2 req
            let req = {
                resourceName: "authzGroup",
                reqUser: user.code,
                order: [{ id: "ASC" }],
                filter: null
            }

            // 1.3 options
            let createResourceOpts = {}

            // 2. execute test

            let rsp = await lookupResourcesService.process(trans, req, createResourceOpts);

            // 3. assert result 
            // 3.1 check returned rsp

            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, 'A')            
            assert.ok(rsp.resources)
            assert.strictEqual(rsp.resources.length, 4);

            let previous: any = null;
            for (let resource of rsp.resources) {

                if (previous) {

                    assert.ok(resource.id > previous.id)
                }

                previous = resource;
            }

        });

});
