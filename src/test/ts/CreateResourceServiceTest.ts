// libs
import * as assert from "assert";
import { Chance } from "chance";
import Container from "@yinz/container/ts/Container";
import { TypeormDao, TypeormDataSource } from "@yinz/commons.data";
import { YinzConnection, YinzTransaction } from "@yinz/commons.data/ts/DataSource";
import Authz from "@yinz/access.data/models/Authz";
import Profile from "@yinz/access.data/models/Profile";
import User from "@yinz/access.data/models/User";
import CreateResourceService from "../../main/ts/CreateResourceService";
import AuthzGroup from "@yinz/access.data/models/AuthzGroup";
import ProfileAuthz from "@yinz/access.data/models/ProfileAuthz";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const chance = new Chance();
// const Exception = container.getClazz('Exception');

const dataSource = container.getBean<TypeormDataSource>('typeormDataSource');

const authzDao = container.getBean<TypeormDao<Authz>>('authzDao');
const authzGroupDao = container.getBean<TypeormDao<AuthzGroup>>('authzGroupDao');
const userDao = container.getBean<TypeormDao<User>>('userDao');
const profileDao = container.getBean<TypeormDao<Profile>>('profileDao');
const profileAuthzDao = container.getBean<TypeormDao<ProfileAuthz>>('profileAuthzDao');

const createResourceService = container.getBean<CreateResourceService>("createResourceService");


let conn: YinzConnection;
let trans: YinzTransaction;

// authz
let executeServiceAuthz; // = { code: 'CreateResourceService' } as Authz;
let createResourceAuthz; // = { code: 'AuthzGroup' } as Authz;
let createResourceUser; // = { code: 'AuthzGroup' } as Authz;

// profiles
let grantService // = { code: chance.string({ length: 10 }) } as Profile;
let grantDao // = { code: chance.string({ length: 10 }) } as Profile;
let grantNone // = { code: chance.string({ length: 10 }) } as Profile;

let user /* = {
    code: chance.string({ length: 10 }),
    status: chance.pick(['A', 'I', 'L']),
} as User; */

const createAuthzNdProfiles = async (trans) => {

    // init 
    // authz
     executeServiceAuthz = { code: 'CreateResourceService' } as Authz;
     createResourceAuthz = { code: 'AuthzGroup' } as Authz;
     createResourceUser = { code: 'User' } as User;

    // profiles
    grantService = { code: chance.string({ length: 10 }) } as Profile;
    grantDao = { code: chance.string({ length: 10 }) } as Profile;
    grantNone = { code: chance.string({ length: 10 }) } as Profile;

    user = {
        code: chance.string({ length: 10 }),
        status: chance.pick(['A', 'I', 'L']),
    } as User;

    // create authzs
    executeServiceAuthz = await authzDao.create(trans, executeServiceAuthz, { user: "__super_user__" });
    createResourceAuthz = await authzDao.create(trans, createResourceAuthz, { user: "__super_user__" });
    createResourceUser = await authzDao.create(trans, createResourceUser, { user: "__super_user__" });
    
    // create profiles
    grantService = await profileDao.create(trans, grantService, { user: "__super_user__", include: ['authzs'] });
    grantDao = await profileDao.create(trans, grantDao, { user: "__super_user__", include: ['authzs'] });
    grantNone = await profileDao.create(trans, grantNone, { user: "__super_user__", include: ['authzs'] });

    // grant service    
    grantService.profileAuthzs = createResourceAuthz.profileAuthzs = [(await profileAuthzDao.create(trans, {
        profile: grantService,
        authz: executeServiceAuthz,
        allowAll: true
    }, {user: "__super_user__", include: ['profile', 'authz']}))]

    // grant dao    
    grantDao.profileAuthzs = createResourceAuthz.profileAuthzs = (await profileAuthzDao.createAll(trans, [
        { profile: grantDao, authz: createResourceAuthz, allowAll: true } ,
        { profile: grantDao, authz: createResourceUser, allowAll: true } ,
    ], { user: "__super_user__", include: ['profile', 'authz'] }))
          
}

describe('| commons.resources <CreateResourceService>', function () {


   

    before(() => {
        return new Promise(async (resolve) => {
            conn = await dataSource.getConn();
            resolve(conn);
        })
    });

    after(() => {
        return new Promise(async (resolve) => {
            await dataSource.relConn(conn)
            resolve(true);
        })
    });

    beforeEach(() => {
        return new Promise(async (resolve) => {
            trans = await dataSource.startTrans(conn);

            await createAuthzNdProfiles(trans);

            resolve(trans);
        })
    });

    afterEach(() => {
        return new Promise(async (resolve) => {
            await dataSource.rollbackTrans(trans)
            resolve(true);
        })
    });



    it('\n' +
        '       | Conditions: \n' +
        '       | - resourceName is valid \n' +
        '       | - resource is valid \n' +
        '       | - reqUser is valid  \n' +                
        '       | - user is allowed to execute service  \n' +                
        '       | - user is allowed to create resource  \n' +                
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create the resource.  \n' +
        '', async () => {

            // 1. prepare test-data 
            
            // 1.1 create user
            user.profiles = [grantService, grantDao]

            await userDao.create(trans, user, {user: "__super_user__", include: ['profiles']});

            // await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']})                        
          
            // 1.2 req
            let req = {
                resourceName: "authzGroup",
                resource: {
                    code: chance.string({length: 10})                    
                },
                reqUser: user.code
            }

            // 1.3 options
            let createResourceOpts = {}

            // 2. execute test

            let rsp = await createResourceService.process(trans, req, createResourceOpts);

            // 3. assert result 
            // 3.1 check returned rsp

            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, 'A')            
            assert.ok(rsp.resource)
            assert.ok(rsp.resource.hasOwnProperty('id'))
            assert.strictEqual(rsp.resource.code, req.resource.code);


            // 3.2 check authzgroup

            let authzGrpResult = await authzGroupDao.findAll(trans, {user: "__super_user__"});

            assert.ok(authzGrpResult)
            assert.strictEqual(authzGrpResult.length , 1)
            assert.strictEqual(authzGrpResult[0].code , req.resource.code)

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - resourceName is valid \n' +
        '       | - resource is valid \n' +
        '       | - resource assoc has an id \n' +
        '       | - reqUser is valid  \n' +
        '       | - user is allowed to execute service  \n' +
        '       | - user is allowed to create resource  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create the resource.  \n' +
        '       | -> should link resource to assoc.  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create user
            user.profiles = [grantService, grantDao]

            await userDao.create(trans, user, { user: "__super_user__", include: ['profiles'] });

            // await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']})                        

            // 1.2 req
            let req = {
                resourceName: "user",
                resource: {
                    code: chance.string({ length: 10 }),
                    status: 'A',
                    profiles: [{id: grantService.id}]

                },
                reqUser: user.code
            }

            // 1.3 options
            let createResourceOpts = {}

            // 2. execute test

            let rsp = await createResourceService.process(trans, req, createResourceOpts);

            // 3. assert result 
            // 3.1 check returned rsp

            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, 'A')            
            assert.ok(rsp.resource)
            assert.ok(rsp.resource.hasOwnProperty('id'))
            assert.strictEqual(rsp.resource.code, req.resource.code);
            assert.strictEqual(rsp.resource.status, req.resource.status);
            assert.ok(rsp.resource.profiles);


            // 3.2 check user

            let userResult = await userDao.findSingleByFilter(trans, {code: req.resource.code}, { user: "__super_user__", include: ['profiles'] });


            assert.ok(userResult)            
            assert.strictEqual(userResult && userResult.code, req.resource.code)
            assert.strictEqual(userResult && userResult.status, req.resource.status)
            assert.strictEqual(userResult && userResult.profiles.length, 1)
            assert.strictEqual(userResult && userResult.profiles[0].code, grantService.code)

        });


    it('\n' +
        '       | Conditions: \n' +
        '       | - resourceName is valid \n' +
        '       | - resource is valid \n' +
        '       | - reqUser is valid  \n' +
        '       | - user is allowed to execute service  \n' +
        '       | - user is allowed to create resource  \n' +
        '       | Expected :  \n' +
        '       | -> should return rspCode A(ccepted).  \n' +
        '       | -> should create the resource.  \n' +
        '       | -> should link resource to assoc.  \n' +
        '', async () => {

            // 1. prepare test-data 

            // 1.1 create user
            user.profiles = [grantService, grantDao]

            await userDao.create(trans, user, { user: "__super_user__", include: ['profiles'] });

            // await userDao.findAll(trans, {user: "__super_user__", include: ['profiles']})                        

            // 1.2 req
            let req = {
                resourceName: "user",
                resource: {
                    code: chance.string({ length: 10 }),
                    status: 'A',
                    profiles: [{ id: grantService.id }]

                },
                reqUser: user.code
            }

            // 1.3 options
            let createResourceOpts = {}

            // 2. execute test

            let rsp = await createResourceService.process(trans, req, createResourceOpts);

            // 3. assert result 
            // 3.1 check returned rsp

            assert.ok(rsp)
            assert.strictEqual(rsp.rspCode, 'A')            
            assert.ok(rsp.resource)
            assert.ok(rsp.resource.hasOwnProperty('id'))
            assert.strictEqual(rsp.resource.code, req.resource.code);
            assert.strictEqual(rsp.resource.status, req.resource.status);
            assert.ok(rsp.resource.profiles);


            // 3.2 check user

            let userResult = await userDao.findSingleByFilter(trans, { code: req.resource.code }, { user: "__super_user__", include: ['profiles'] });


            assert.ok(userResult)
            assert.strictEqual(userResult && userResult.code, req.resource.code)
            assert.strictEqual(userResult && userResult.status, req.resource.status)
            assert.strictEqual(userResult && userResult.profiles.length, 1)
            assert.strictEqual(userResult && userResult.profiles[0].code, grantService.code)

        });



});
