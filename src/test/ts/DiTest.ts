import * as assert from "assert";
import * as stringify from "json-stringify-safe";
import Container from "@yinz/container/ts/Container";


const container = new Container({
    paramsFile: 'params.yml',
    modules: [
        '@yinz/commons/ts',
        '@yinz/commons.data/ts',
        '@yinz/access.data/ts',
        '@yinz/commons.services/ts',
        process.cwd() + '/dist/main/ts'
    ],
});

const Exception = container.getClazz('Exception');


describe('| commons.resources.di', function () {
    it('| should assemble the module', function () {
        try {
            // classes
            assert.ok(container.getClazz('ResourceUtils'));
            assert.ok(container.getClazz('CreateResourceService'));            
            assert.ok(container.getClazz('LookupResourceService'));            
            assert.ok(container.getClazz('UpdateResourceService'));            
            assert.ok(container.getClazz('DeleteResourceService'));            
            assert.ok(container.getClazz('LookupResourcesService'));            


            // beans
            assert.ok(container.getBean('resourceUtils'));
            assert.ok(container.getBean('createResourceService'));            
            assert.ok(container.getBean('lookupResourceService'));            
            assert.ok(container.getBean('updateResourceService'));            
            assert.ok(container.getBean('deleteResourceService'));            
            assert.ok(container.getBean('lookupResourcesService'));            

        } catch (e) {
            if (e instanceof Exception) {
                assert.ok(false, 'exception: ' + e.reason + ': ' + stringify(e.extra));
            }
            else {
                assert.ok(false, 'exception: ' + e.message + '\n' + e.stack);
            }
        }
    });
});
